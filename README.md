# Tools for SDETs [Software Developers in Test]

## Selenium
Often the most important part of any application/business are it's UIs. However, they happen to be the most prone to problems especially for the webpages. So testing a webpage at every level of development is critical to ensure quality control.

To make testing less costly and more proactive, we automate it. *Yet, how do you open/manipulate a webpage with code?*

One solution is [Selenium](https://www.seleniumhq.org/). Selenium basically provides a set of browser agnostic APIs to manipulate a webpage to run test scenarios at integration/e2e/higher level testing manner. It has libraries in most languages particularly Java.

#### Why would you want these APIs to be browser agnostic?
If you have to support Firefox, Chrome, Opera, ... then you want to write that code once and move on. Selenium provides a way to run your test against several browser all at once.

You can see an example under the `selenium` folder.


## Cucumber

By now, you should be familiar with *BDD*, but what does that look like from a coding perspective?

If you see from the title or the tone, Cucumber is one popular solution. 

### Now, what is cucumber?

Cucumber is a way to extract testing into plain english for the purpose of managing business requirements with code. It's essentially a test runner similar to JUnit. To do high-level integration/e2e test, you'll need selenium for browser automation.

### Cucumber Java: How does it work?

I've included a simple exercise under the `cucumber` folder.


You can run the example by `cd`ing into the directory and running the *maven* test command: `mvn test`. You can configure your IDE to run them as well, but it is too time consuming for this tutorial.

The actual tests are still using the JUnit runner and assertions. Cucumber sits as a plugin into JUnit via the `@RunWith(Cucumber.class)` attribute.

 - Also note, you need to need to place this on a class called `RunCucumberTest`. Several hours were spent debugging why the tests weren't running. The only issue was naming the entrypoint something other than `RunCucumberTest`.

#### What are `.features` files, and where do I put them?

The `.feature` files are simple text files that hold gherkin steps. Cucumber reads them then tries to connect a particular step to a piece of code at runtime.

In Java you place them in your `resource` folder under the package where the test code lives. The example provided has a folder structure of `resources/com.chathamfinancial.zipcode.SDET.cucumber`. The package being `com.chathamfinancial.zipcode.SDET.cucumber`. 

The package is important for cucumber to bind the test code together.

#### State and Encapsulation in Cucumber Java

There are tons of design principals for implementing gherkin steps, but we will not be covering those. We will be covering the idea of *context* classes and passing state among steps.

You'll notice we have a class called `ScenarioContext`. This class is being injected into the constructor by a __magical__ but secret IOC container in the `pom.xml` file.

The example shows this:
```
    public Stepdefs(ScenarioContext context) {
        this.context = context;
    }
```

If you haven't covered IOC, please read this post by [Martin Folwer](https://martinfowler.com/articles/injection.html) (a.k.a. someone very respected and important in software development).

The secret IOC container is a cucumber java plugin called [cucumber-picocontainer](https://github.com/cucumber/cucumber-jvm/tree/master/picocontainer). It is a simple dependency in the  `pom.xml` file if you need to swap it out for whatever reason.

```
    <dependency>
        <groupId>io.cucumber</groupId>
        <artifactId>cucumber-picocontainer</artifactId>
        <version>2.3.1</version>
        <scope>test</scope>
    </dependency>
```

The library looks for dependencies being injected into classes and tries to provide them. The example's `ScenarioContext` is injected by these means. We use it to hold onto the selenium driver instance so we can use it in the definitions and also close the browser window at the end of the test. To look at the close procedure, open the `Hooks` class. 

- Hook files are just a cucumber convention for setup and teardown procedures.

#### State between steps should be encapsulated within a context class. **You should never save any data in the hooks or step definition classes.**
